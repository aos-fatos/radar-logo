from setuptools import setup, Extension

setup(
    name='radarlogodetect',
    version='0.0.1',
    description="Logo radar detector, detect specific logos with .99 of accuracy based on our database",
    author='Heitor Sampaio',
    author_email='horlando.heitor@gmail.com',
    license='MIT',
    packages=['radarlogodetect'],
    include_package_data=True,
    package_dir={'radarlogodetect': 'radarlogodetect'},
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    install_requires=['tensorflow'],
    python_requires='>=3',
)
