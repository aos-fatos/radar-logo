import pickle
import os
import numpy as np
import tensorflow.keras

from PIL import Image, ImageOps

class Logoradar:

    def __init__(self):
        base_path = os.path.dirname(os.path.abspath(__file__))
        self.model = tensorflow.keras.models.load_model(base_path + '/data/keras_model.h5')
        self.classes = []
        labels_path = base_path + '/data/labels.txt'
        labelsfile = open(labels_path, 'r')
        line = labelsfile.readline()
        while line:
            self.classes.append(line.split(' ', 1)[1].rstrip())
            line = labelsfile.readline()
        labelsfile.close()
        

    def predict(self, image):
        confidence = []
        conf_label = ''
        threshold_class = ''
        data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)
        image = Image.open(image)
        size = (224, 224)
        image = ImageOps.fit(image, size, Image.ANTIALIAS)
        image_array = np.asarray(image)
        normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1
        data[0] = normalized_image_array
        prediciton = self.model.predict(data)
        for i in range(0, len(self.classes)):
            confidence.append(int(prediciton[0][i]*100))
            conf_label += self.classes[i] + ': ' + str(confidence[i]) + '%;'
            if confidence[i] > 90:
                threshold_class = self.classes[i]
                return self.classes[i]
            else:
                threshold_class = ''

        return threshold_class
